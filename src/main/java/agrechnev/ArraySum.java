package agrechnev;

public class ArraySum{

    public static class NullArrayException extends Exception{

    }

    int[] array;

    public ArraySum(int[] array) {
        this.array = array;
    }

    public static int Sum(int[] array) throws NullArrayException{
        int sum=0;

        if (array==null) throw new NullArrayException();

        for (int i:array) {
            sum+=i;
        }
        return sum;
    }

    public int Sum() throws NullArrayException{
        return Sum(array);
    }
}