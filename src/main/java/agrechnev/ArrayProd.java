package agrechnev;

public class ArrayProd{

    public static class NullArrayException extends Exception{

    }

    int[] array;

    public ArrayProd(int[] array) {
        this.array = array;
    }

    public static int Prod(int[] array) throws NullArrayException{
        int prod=1;

        if (array==null) throw new NullArrayException();

        for (int i:array) {
            prod*=i;
        }
        return prod;
    }

    public int Prod() throws NullArrayException{
        return Prod(array);
    }
}