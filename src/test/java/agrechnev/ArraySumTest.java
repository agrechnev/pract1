package agrechnev;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArraySumTest{

    private ArraySum as1;

    @Before
    public void beforeTest(){
        System.out.println("Before test !");
        int[] myArr={1,2,3};

        as1=new ArraySum(myArr);
    }

    @After
    public void afterTest(){
        System.out.println("After test!");
    }

    @Test
    public void testSum1() throws ArraySum.NullArrayException{
        int[] arr={7,3,15};
        assertEquals(ArraySum.Sum(arr),25);
    }

    @Test
    public void testSum2() throws ArraySum.NullArrayException{
        assertEquals(as1.Sum(),6);
    }

    @Test(expected=ArraySum.NullArrayException.class)
    public void testSum3()  throws ArraySum.NullArrayException{
        int[] arr={7,3,15};
        int sum=ArraySum.Sum(arr);
        sum=ArraySum.Sum(null);
    }
}