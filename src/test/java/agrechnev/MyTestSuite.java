package agrechnev;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ArraySumTest.class,
        ArrayProdTest.class,
        StudentTest.class
})
public class MyTestSuite{

}