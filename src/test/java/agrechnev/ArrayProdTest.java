package agrechnev;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ArrayProdTest{
    @Test
    public void testProd()  throws ArrayProd.NullArrayException{
        int[] arr={1,2,3,4};

        assertEquals(ArrayProd.Prod(arr),24);
    }
}